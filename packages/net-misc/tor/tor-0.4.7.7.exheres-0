# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'tor-0.2.0.32-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require systemd-service
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

SUMMARY="Anonymizing Internet communication system"
DESCRIPTION="
Tor helps you defend against traffic analysis, a form of network surveillance
that threatens personal freedom and privacy, confidential business activities
and relationships, and state security. Tor protects you by bouncing your
communications around a distributed network of relays run by volunteers all
around the world: it prevents somebody watching your Internet connection from
learning what sites you visit, and it prevents the sites you visit from learning
your physical location. Tor works with many of your existing applications,
including web browsers, instant messaging clients, remote login, and other
applications based on the TCP protocol.
"
HOMEPAGE="http://www.torproject.org/"
DOWNLOADS="http://www.torproject.org/dist/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug [[ description = [ Compile with debugging info ] ]]
    systemd [[ description = [ Enable systemd notification support ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/automake[>=1.7]
    build+run:
        app-arch/zstd[>=1.1]
        dev-libs/libevent:=[>=1.2]
        dev-libs/libpcap
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.1] )
        sys-libs/libseccomp
        systemd? ( sys-apps/systemd )

        group/tor
        user/tor

    suggestion:
        app-admin/logrotate [[ description = [ Use logrotate for rotating logs ] ]]
        net-proxy/polipo    [[ description = [
            Tor-friendly HTTP proxy, recommended by upstream over Privoxy
        ] ]]
        net-proxy/torsocks  [[ description = [
            Tor-friendly SOCKS proxy, recommended by upstream over tsocks
        ] ]]
    test:
        dev-lang/python:*[>=3.0.0]
"

REMOTE_IDS="freecode:${PN}"
UPSTREAM_CHANGELOG="https://git.torproject.org/checkout/tor/master/ChangeLog [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="https://www.torproject.org/docs/tor-doc-unix.html.en [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://git.torproject.org/checkout/tor/master/ReleaseNotes [[ lang = en ]]"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p1 "${FILES}"/torrc.sample.in-sane-defaults.patch
    -p1 "${FILES}"/tor.logrotate.in.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-lzma
    --enable-zstd
    --localstatedir=/var
    --with-tor-user=tor
    --with-tor-group=tor
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug systemd )

DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( doc/ )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    ReleaseNotes
    doc/spec/{{address,control,dir,path,rend,tor,version}-spec,socks-extensions}.txt
)

src_prepare() {
    default
    edo sed -e 's/ar x/"${AR:-ar}" x/g' -i scripts/build/combine_libs
}

src_install() {
    default

    keepdir /var/{lib,log}/tor
    edo chown tor:tor "${IMAGE}"/var/{lib,log}/tor
    edo chmod 750 "${IMAGE}"/var/{lib,log}/tor

    install_systemd_files
    install_openrc_files

    insinto /etc/logrotate.d
    newins contrib/operator-tools/tor.logrotate tor

    # Allow user 'tor' more open file handles to avoid errors, see Gentoo bug 251171
    insinto /etc/security/limits.d/
    doins "${FILES}"/tor.conf
}

pkg_postinst() {
    elog "Before using Tor you need a configuration in /etc/tor/torrc. An example can be"
    elog "found in /etc/tor/torrc.sample."
}

