# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake python [ blacklist=2 multibuild=false ]

SUMMARY="A tool to test PAM applications and PAM modules"
DESCRIPTION="
* Allows you to test PAM actions such as user logins in applications.
* Simplifies testing of PAM modules.
* Provides a C library and python bindings to write simpler tests with less code
"
HOMEPAGE="https://cwrap.org/${PN}.html"
DOWNLOADS="mirror://samba/../cwrap/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-libs/pam
    test:
        dev-util/cmocka[>=1.1.0]
"

src_configure() {
    local cmakeargs=(
        -DPYTHON3_INCLUDE_DIR=$(python_get_incdir)
        -DPYTHON3_LIBRARY=/usr/$(exhost --target)/lib/libpython$(python_get_abi).so
        -DPYTHON3_SITELIB=$(python_get_sitedir)
        $(expecting_tests -DUNIT_TESTING:BOOL=TRUE -DUNIT_TESTING:BOOL=FALSE)
    )

    ecmake "${cmakeargs[@]}"
}

